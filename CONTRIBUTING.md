# Contributing

## Process
 1. Fork the project
 2. Work on your modification
 3. Apply Standard ``standard --fix``
 4. Create Merge request back in
 
 
## Running the project
This is a node.js server based project so the basics from that apply.  

### Requirements
* MongoDB 4.0+
* Node.js 11.6.0+

### Instructions
1. Copy the ``data.json.example`` to ``data.json`` and fill in the details in it.
2. ``yarn`` or ``npm install`` to install the base components.  
3. ``yarn start`` or ``npm start`` to bring up the development server running on the port specified in the ``data.json``.  

## Notes
* If you have any queries on whether it will be accepted or not please contact me ahead of time.