# Gw2 API Parser.

This is the source code for my parser that outputs itemdata and prices (as well as a whole host more)

## Usage
Please look at the [Wiki](https://gitlab.com/Silvers_Gw2/Market_Data_Processer/-/wikis/) for more info


## Selfhosting it

Please refer to [Contributing Document](https://gitlab.com/Silvers_Gw2/Market_Data_Processer/blob/master/CONTRIBUTING.md)

## Contact
If you have any suggestions just ask and I will try to incorporate them in.

* Silver#5563 on Discord
* Silveress_Golden on reddit  
* [Discord server](https://discord.gg/m3G33ur)