import axios, {AxiosRequestConfig} from 'axios'
import {proxy, name} from "../config/config"

export default class Proxy{
  useProxy = false

  constructor() {
    // inly use the proxy if its set
    if(proxy !== ""){
      // first test it
      let testConfig:AxiosRequestConfig = {
        method: 'get',
        url: proxy,
        headers:{
          "proxy-conf": JSON.stringify({url: "https://api.guildwars2.com/v2/quaggans", category:"guildwars2", program:name})
        }
      }

      axios(testConfig)
          // set the useProxy parm to true if its positive
          .then(response => {
            if (response.status === 200) {this.useProxy = true}
          })
          .catch(()=> console.log("Proxy Failed"))
    }
  }

  get(input: string | AxiosRequestConfig, error_res?: string) {
    if (typeof input !== "string" && typeof input !== "object") {
      return;
    }

    let config = this.generateConfig(input)

    return axios(config)
    .then((res) => res)
    .catch(() => {
      if (error_res) {
        throw error_res;
      } else {
        throw input
      }
    });
  }

  private generateConfig(input: string | AxiosRequestConfig): AxiosRequestConfig{
    let config:AxiosRequestConfig

    if(this.useProxy){
      // using teh proxy

      let proxyConf = {}

      // singel parameter is a url
      if(typeof input === "string"){
        proxyConf["url"] = encodeURI(input)
      }else{
        proxyConf["config"] = input
        proxyConf["config"]["url"] = encodeURI(proxyConf["config"]["url"])
      }
      proxyConf["category"] = "guildwars2"
      proxyConf["program"] = name

      config = {
        method: 'get',
        url: proxy,
        headers:{
          "proxy-conf": JSON.stringify(proxyConf)
        }
      }
    }else{
      if(typeof input === "string"){
        config = {
          method: 'get',
          url: encodeURI(input),
        }
      }else{
        config = input
        config["url"] = encodeURI(config["url"])
      }
    }

    return config
  }
}