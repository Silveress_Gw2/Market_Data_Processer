import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Gem_Store_Catalog, Gem_Store_Categories, Gem_Store_Index} from "../config/db_entities"

export const Gemstore = (ctx: Ctx) =>  {
  const {database,server} = ctx;


  let active = { beautify: true, fields: true, project: true, filter: true, ids: false, itemID: false, start: false, end: false }
  let catalogueFields = "uuid,blurb,categories,categoriesOld,categoryLifespansOld,dataId,description,excludedFeatures,firstAdded,giftable,hide,imageHash,itemGuid,latestEndDate,latestStartDate,name,randomchance,requiredFeatures,requirementMissing,returning,type,gemPrice,categoryLifespans,contentGuid,contents,sku,passwords,availableDate,gemStoreBannerItem,removeDate"
  let categoriesFields = "uuid,addendum,childCategories,description,displayname,featuredItemLifespan,featuredItems,firstAdded,name,parentCategories,childLifespans"
  let indexFields = "build,items"


  server.get('/gw2/v1/gemstore/catalogue/json', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Catalog, req.url, active, catalogueFields, undefined, undefined, { firstAdded: -1 })
    next()
  })

  server.get('/gw2/v1/gemstore/catalogue/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Catalog, req.url, active, catalogueFields, 'key')
    next()
  })

  server.get('/gw2/v1/gemstore/catalogue/feed/:type', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Catalog, req.url, active, catalogueFields, "feed", undefined, {firstAdded: -1})
    next()
  })

  server.get('/gw2/v1/gemstore/categories/json', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Categories, req.url, active, categoriesFields, undefined, undefined, { firstAdded: -1 })
    next()
  })

  server.get('/gw2/v1/gemstore/categories/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Categories, req.url, active, categoriesFields, 'key')
    next()
  })

  server.get('/gw2/v1/gemstore/index/json', async (req, res, next) => {
    await defaultPath(database, req, res, Gem_Store_Index, req.url, active, indexFields, undefined, undefined, { build: -1 })
    next()
  })
}
