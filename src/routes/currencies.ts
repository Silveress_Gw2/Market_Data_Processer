import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Currencies as Currencies_Type} from "../config/db_entities"

export const Currencies = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'id,description,firstAdded,icon,name,order'

  server.get('/gw2/v1/currencies', async (req, res, next) => {
    await defaultPath(database, req, res, Currencies_Type, req.url, active, defaultFields)
    next()
  })

  server.get('/gw2/v1/currencies/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Currencies_Type, req.url, active, defaultFields, 'key')
    next()
  })
}
