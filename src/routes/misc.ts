import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Festivals, Raid_Marketable} from "../config/db_entities"

export const Misc = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: false, itemID: false, start: false, end: false }
  let defaultFields = 'row,cost,currency,gold,id'

  server.get('/gw2/v1/misc/raid/rewards', async (req, res, next) => {
    await defaultPath(database, req, res, Raid_Marketable, req.url, active, defaultFields, undefined, undefined, { row: 1 })
    next()
  })

  server.get('/gw2/v1/misc/raid/rewards/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Raid_Marketable, req.url, active, defaultFields, 'key')
    next()
  })

  server.get('/gw2/v1/misc/festival', async (req, res, next) => {
    await defaultPath(database, req, res, Festivals, req.url, active, "festival,year,start,end,type,id,quantity", undefined, undefined, { row: 1 })
    next()
  })

  server.get('/gw2/v1/misc/festival/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Festivals, req.url, active, defaultFields, 'key')
    next()
  })
}
