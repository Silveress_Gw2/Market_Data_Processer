import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {All_Info, Item} from "../config/db_entities"

export const Items = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'id,name,lastUpdate,buy_quantity,buy_price,sell_quantity,sell_price'
  let defaultFind = { name: { $exists: true }, marketable: true }

  server.get('/gw2/v1/items/csv', async (req, res, next) => {
    await defaultPath(database, req, res, All_Info, req.url, active, defaultFields, 'csv', defaultFind)
    next()
  })

  server.get('/gw2/v1/items/json', async (req, res, next) => {
    await defaultPath(database, req, res, All_Info, req.url, active, defaultFields, undefined, defaultFind)
    next()
  })

  /*
  special csv output field for google sheets
  not perfect but it does reduce teh filesize by 1/8 and cells by 2/7
  I am unsure which of these gsheets uses in deciding limits
   */
  server.get('/gw2/v1/items/g_csv', async (req, res, next) => {
    let fields =  'id,name,lastUpdate,buy_price,sell_price';
    await defaultPath(database, req, res, All_Info, req.url, active, fields, 'csv', defaultFind)
    next()
  })

  server.get('/gw2/v1/items/feed/:type', async (req, res, next) => {
    await defaultPath(database, req, res, All_Info, req.url, active, defaultFields, "feed", undefined, {firstAdded: -1})
    next()
  })

  server.get('/gw2/v1/items/json/:sorting', async (req, res, next) => {
    let sorting = req.params.sorting.toString().toLowerCase()
    let defaultSort = {}
    switch (sorting) {
      case 'new': { defaultSort['firstAdded'] = -1; break }
      case 'old': { defaultSort['firstAdded'] = 1; break }
      default: { defaultSort['id'] = 1 }
    }
    await defaultPath(database, req, res, All_Info, req.url, active, defaultFields, undefined, defaultFind, defaultSort)
    next()
  })

  server.get('/gw2/v1/items/keys', async (req, res, next) => {
    await defaultPath(database, req, res, All_Info, req.url, active, defaultFields, 'key')
    next()
  })

  let defaultFields_v2 = "id,chat_link,default_skin,description,description_de,description_es,description_fr,details,firstAdded,lastUpdate,flags,game_types,icon,level,name,name_de,name_es,name_fr,rarity,restrictions,type,upgrades_from,upgrades_into,vendor_value"
  server.get('/gw2/v1/item_data/json', async (req, res, next) => {
    await defaultPath(database, req, res, Item, req.url, active, defaultFields_v2)
    next()
  })

  server.get('/gw2/v1/item_data/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Item, req.url, active, defaultFields_v2, 'key')
    next()
  })
}
