import {Ctx} from "../Gw2_API";
import {defaultPath} from '../JS/functions';
import {Skins as Skins_Type} from "../config/db_entities"

export const Skins = (ctx: Ctx) =>  {
  const {database,server} = ctx;

  let active = { beautify: true, fields: true, project: true, filter: true, ids: true, itemID: false, start: false, end: false }
  let defaultFields = 'id,details,flags,icon,name,rarity,restrictions,type'

  server.get('/gw2/v1/skins/json', async (req, res, next) => {
    await defaultPath(database, req, res, Skins_Type, req.url, active, defaultFields)
    next()
  })

  server.get('/gw2/v1/skins/keys', async (req, res, next) => {
    await defaultPath(database, req, res, Skins_Type, req.url, active, defaultFields, 'key')
    next()
  })
}
