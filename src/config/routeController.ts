import {Ctx} from "../Gw2_API"
import {Recipes} from "../routes/recipes"
import {Items} from "../routes/items"
import {History} from "../routes/history"
import {Status} from "../routes/status"
import {Skins} from "../routes/skins"
import {Achievements} from "../routes/achievements"
import {Currencies} from "../routes/currencies"
import {Misc} from "../routes/misc"
import {Build} from "../routes/build"
import {Gemstore} from "../routes/gemstore"
import {Gems} from "../routes/gems";

export const routes = (ctx: Ctx) => {
  Recipes(ctx)
  Items(ctx)
  History(ctx)
  Status(ctx)
  Skins(ctx)
  Achievements(ctx)
  Currencies(ctx)
  Misc(ctx)
  Build(ctx)
  Gemstore(ctx)
  Gems(ctx)
}
