import * as csv from 'fast-csv';
const JsonStreamStringify = require('json-stream-stringify');
let Feed = require('feed').Feed;
import restify from "restify"

import {DB_Manager} from "../config/db";
import Proxy from "../classes/Proxy";
import {Filter} from "mongodb";

const functions = {
  // used for teh JS modules
  defaultPath: defaultPath,
}
export default functions

// TODO: Darth has a new URL
/*
  General functions
 */
export function getDarthsCDN (url: string): string {
  // using Darth's CDN
  let imgRegex = /https:\/\/render\.guildwars2\.com\/file\/(.*?)\.png/
  let imgDarth = 'https://icons-gw2.darthmaim-cdn.com/$1.png'
  if (typeof url === 'string') {
    return url.replace(imgRegex, imgDarth)
  } else {
    return ''
  }
}

export function array_to_keyed_object<T>(array:T[],key: string ): {[propName: string]: T}{
  let tmp = {};
  for(let i=0;i<array.length;i++){
    tmp[array[i][key]] = array[i]
  }
  return tmp
  }

export function chunkArray<T> (array:Array<T>, perChunk: number): Array<Array<T>>{
  if(typeof perChunk !== "number"){return [array]}
  let output = []
  let tmp = []
  for(let i=0;i<array.length;i++){
    if(tmp.length < perChunk){
      tmp.push(array[i])
    }else{
      output.push(tmp)
      tmp = [array[i]]
    }
  }
  if(tmp.length > 0){output.push(tmp)}
  return output
}

export function beautifyJSON (json: any, flag: string): string {
  if (flag === 'min') {
    return JSON.stringify(json)
  } else {
    return JSON.stringify(json, null, '  ')
  }
}

export function dateAgo(type: string, quantity: number, customStart?: string, seconds?: number){
  let date = new Date()
  if(typeof type !== "string"){return date.toISOString()}
  if(customStart){date = new Date(customStart)}

  switch(type){
    case "minutes":{
      date.setMinutes(date.getMinutes() - quantity)
      date.setUTCSeconds(0, 0)
      break
    }
    case "hours":{
      date.setHours(date.getHours() - quantity)
      date.setUTCMinutes(0, 0, 0)
      break
    }
    case "days":{
      date.setDate(date.getDate() - quantity)
      date.setUTCHours(0, 0, 0, 0)
      break
    }
    default:{}
  }

  if(seconds){
    date.setUTCSeconds(seconds, 0)
  }
  return date.toISOString()
}

export function sorter (a: number | string, b: number | string, reverse: boolean = false) {
  if (reverse) {
    if (a < b) { return 1 }
    if (a > b) { return -1 }
  } else {
    if (a < b) { return -1 }
    if (a > b) { return 1 }
  }
  return 0
}

export async function get_endpoint_data<T>(ids_strings: string[], baseURL: string, proxyFunction: Proxy, perChunk: number = 20): Promise<T[]> {
  let details: T[] = []

    let promises = ids_strings.map(
      (idStr) => proxyFunction.get(`${baseURL}?ids=${idStr}`, idStr)
    );

    await Promise.allSettled(promises).then((results) =>
      results.forEach((result) => {
        switch (result.status) {
          case "fulfilled":
            if (result.value.data.length > 0) {
              details.push(...result.value.data)
            }
            break;
          case "rejected":
            console.error(`Failed to fetch ID's: ${result.reason}`)
            break;
        }
      }),
    );

  return details
}

/* 
  Math functions
 */
export function getSD (data: number[]) {
  if(data.length === 0){
    return 0
  }
  const mean = data.reduce((a, b) => a + b) / data.length
  return Math.sqrt(
      data.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / data.length
  )
}

export function arrayAvg (arr: number[]) {
  return arr.reduce((a, b) => a + b, 0) / arr.length
}

/*
  Manage incoming/outgoing requests
*/

interface Active {
  beautify: boolean;
  fields: boolean;
  project: boolean;
  filter: boolean;
  ids: boolean;
  itemID: boolean;
  start: boolean;
  end: boolean;
}

interface Query {
  beautify?: string;
  fields?: string;
  ids?: string;
  itemID?: string;
  start?: string;
  end?: string;
  filter?: string;

  // catch remainder
  [propName: string]: string;
}
interface QuerySorter {
  beautify: string;
  fields: string;
  project: string[];
  find: Filter<any>
}
export function querySorter (query: Query, active: Active, defaultFields: string, defaultFind: Filter<any>): QuerySorter {
  let result: QuerySorter = {
    beautify: "",
    fields: "",
    find: {
      $and: []
    },
    project: []
  }
  if (active.beautify) {
    result.beautify = query.beautify || 'human'
  }

  result.fields = query.fields || defaultFields || 'id,name,lastUpdate,buy_quantity,buy_price,sell_quantity,sell_price'

  result.project = []
  if (active.project) {
    if(result.fields !== "IAmEvonGnashblade"){
      result.project = result.fields.split(',')
    }
  }

  if (active.ids) {
    if (typeof query.ids !== "undefined") {
      let ids = query.ids.split(',')

      let idQuery = { $or: [] }
      for (let i = 0; i < ids.length; i++) {
        let id = parseInt(ids[i], 10);
        if (isNaN(id)) { continue }
        idQuery.$or.push({ 'id': id })
      }
      if (idQuery.$or.length > 0) {
        result.find.$and.push(idQuery)
      }
    }
  }

  // this is for collections where itemid is used insted of id
  if (active.itemID) {
    if (typeof query.itemID !== "undefined") {
      let ids = query.itemID.split(',')

      let idQuery = { $or: [] }
      for (let i = 0; i < ids.length; i++) {
        let id = parseInt(ids[i], 10);
        if (isNaN(id)) { continue }
        idQuery.$or.push({ 'itemID': id })
      }
      if (idQuery.$or.length > 0) {
        result.find.$and.push(idQuery)
      }
    }
  }

  if (active.start) {
    if (typeof query.start !== 'undefined') {
      result.find.$and.push({ date: { $gte: new Date(query.start).toISOString() } })
    }
  }
  if (active.end) {
    if (typeof query.end !== 'undefined') {
      result.find.$and.push({ date: { $lte: new Date(query.end).toISOString() } })
    }
  }

  if (active.filter) {
    if (result.find.$and.length === 0) {
      if (typeof query.filter !== "undefined") {
        result.find = filterSorter(query.filter)
      }
    } else {
      if (typeof query.filter !== 'undefined') {
        let filterResult = filterSorter(query.filter)
        let keys = Object.keys(filterResult)
        for (let i = 0; i < keys.length; i++) {
          let tmp = {}
          tmp[keys[i]] = filterResult[keys[i]]
          result.find.$and.push(tmp)
        }
      }
    }
  }

  if (typeof result.find.$and !== 'undefined' && result.find.$and.length === 0) {
    if (typeof query.filter !== "undefined") {
      // if it gets here then it means there is essensially no filter, set it here
      result.find = {}
    }else{
      if (typeof defaultFind === 'object') {
        result.find = defaultFind
      }
    }
  }
  return result
}

export function filterSorter (Filter: string): Filter<any> {
  let filter: Filter<any> = {}

  if (Filter.length >= 3 && Filter.indexOf(':') !== -1) {
    let splitFilter = Filter.split(',')
    if (typeof splitFilter === 'object') {
      for (let i = 0; i < splitFilter.length; i++) {
        if (splitFilter[i].length >= 3) {
          let c = splitFilter[i].split(':')
          let field = c[0];
          let searchTerm = c[1]
          let value: string = c[2] || "";

          switch (searchTerm){
            case "TRUE" : {
              filter[field] = true;
              break
            }
            case "FALSE" : {
              filter[field] = false;
              break
            }

            case "gt":
            case "gte":
            case "lt":
            case "lte":
            case "eq":
            case "ne": {
              if(value === ""){continue}
              filter[field] = {
                [`$${searchTerm}`]: parseInt(value, 10)
              }
              break;
            }

            case "in":
            case "nin": {
              if(value === ""){continue}
              filter[field] = {
                [`$${searchTerm}`]: value.split('.')
              }
              break
            }

            case "cts": {
              if(value === ""){continue}
              filter[field] = {
                $regex: value,
                $options: "ix"
              }
              break
            }
            default: {
              filter[field] = searchTerm
            }
          }
        }
      }
    }
  }
  return filter
}

/*
  These manage teh endpoints
 */

interface Objects<T> {
  [propName: string]: T;
}

export async function defaultPath<T> (
    dbManager: DB_Manager,
    req: restify.Request,
    res: restify.Response,
    collection: (new () => T) | string,
    path: string,
    active: Active,
    defaultFields: string,
    modifier?: string,
    defaultFind: Objects<any> = {},
    defaultSort?:Objects<number>,
    defaultHeaders?: string,
    transform?: (row: any) => any
) {

  if (typeof defaultSort === 'undefined') { defaultSort = { id: 1 } }
  let sortedQuery = querySorter(req.query, active, defaultFields, defaultFind)
  let limit = 0
  if(req.query.limit){
    if(!isNaN(req.query.limit -0)){
      limit = req.query.limit -0
    }
  }

  switch (modifier) {
    case 'key': {
      await defaultPathKey (dbManager,res,collection,defaultSort,sortedQuery)
      break
    }
    case 'csv': {
      await defaultPathCsv(dbManager,res,collection,defaultSort,defaultFields,defaultHeaders,sortedQuery, limit, path,transform)
      break
    }
    case "history":{
      await defaultPathHistory(dbManager,res,collection,defaultSort,defaultFields,defaultHeaders,sortedQuery, limit)
      break
    }
    case "feed":{
      let type = req.params.type.toString().toLowerCase()
      await defaultPathFeed(dbManager, res, type, collection, defaultSort, path)
      break
    }
    default: {
      await defaultPathDefault(dbManager,res,collection,defaultSort,defaultFields,defaultHeaders,sortedQuery, limit)
    }
  }
}
//dbManager: DB_Manager
export async function defaultPathDefault<T>(dbManager: DB_Manager, res: restify.Response, collection: (new () => T) | string, defaultSort, defaultFields, defaultHeaders, sortedQuery: QuerySorter, limit: number){
  let result = await dbManager.get_items(collection, sortedQuery.find, sortedQuery.project, limit, 0, defaultSort);
  res.sendRaw(200, beautifyJSON(result, sortedQuery.beautify), { 'Content-Type': 'application/json; charset=utf-8' })
}

export async function defaultPathKey<T>(dbManager: DB_Manager, res: restify.Response, collection: (new () => T) | string, defaultSort, sortedQuery: QuerySorter){
  let keysOutput = await dbManager.get_items(collection, {}, undefined, 3000, 0, defaultSort);
  let keys = {}
  for (let i = 0; i < keysOutput.length; i++) {
    keys =  Object.assign({},keys,keysOutput[i]);
  }
  delete keys["_id"]
  // lazy way to solve this but ah well
  delete keys["lastUpdated"]
  let result = Object.keys(keys)
  res.sendRaw(200, beautifyJSON(result, sortedQuery.beautify), { 'Content-Type': 'application/json; charset=utf-8' })
}

export async function defaultPathCsv<T>(dbManager: DB_Manager, res: restify.Response, collection: (new () => T) | string, defaultSort, defaultFields, defaultHeaders, sortedQuery: QuerySorter, limit: number, path: string, transform: (row: any)=> any){
  let {collection_string} = dbManager.test_collection(collection);

  let project = dbManager.process_project(sortedQuery.project)
  const cursor = dbManager.db.collection(collection_string).find(sortedQuery.find).project(project).sort(defaultSort).limit(limit)
  const filename = `api-datawars2-ie${path.split("?")[0].replace(/\//gi, '_').replace("_csv", '.csv')}`

  // Set approrpiate download headers
  res.setHeader('Content-disposition', `attachment; filename=${filename}`);
  res.writeHead(200, { 'Content-Type': 'text/csv' });

  // Flush the headers before we start pushing the CSV content
  res.flushHeaders();

  // Pipe/stream the query result to the response via the CSV transformer stream
  let options: csv.FormatterOptionsArgs  = {}
  if (typeof defaultFields !== 'undefined' && typeof defaultHeaders === 'undefined') {
    options.headers = defaultFields.split(",")
  }
  let fields = sortedQuery.fields.split(',')
  if(fields.length >0 ){
    options.headers = fields
  }
  if (typeof defaultHeaders !== 'undefined') {
    if(defaultHeaders === "true"){
      options.headers = true
    } else{
      options.headers = defaultHeaders
    }
  }

  // returns everything
  if(sortedQuery.fields === "IAmEvonGnashblade"){
    options.headers = true
  }

  if (typeof transform !== 'undefined') {
    options.transform = transform
  }

  cursor.stream()
  .pipe(csv.format(options))
  .pipe(res)
}

export async function defaultPathHistory<T>(dbManager: DB_Manager, res: restify.Response, collection: (new () => T) | string, defaultSort, defaultFields, defaultHeaders, sortedQuery: QuerySorter, limit: number){
  if(collection === "marketDataArchiveTemp"){
    sortedQuery.find.type = "hour"
  }
  let {collection_string} = dbManager.test_collection(collection);
  let project = dbManager.process_project(sortedQuery.project)
  const cursor = dbManager.db.collection(collection_string).find(sortedQuery.find).project(project).limit(limit).stream();
  // Set approrpiate download headers
  res.setHeader('Content-disposition', 'application/json; charset=utf-8');
  res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
  res.flushHeaders();

  new JsonStreamStringify(cursor).pipe(res);
}

export async function defaultPathFeed<T>(dbManager: DB_Manager, res: restify.Response,type, collection: (new () => T) | string, defaultSort, path){
  let link = "https://api.datawars2.ie" + path.substring(0,path.lastIndexOf("/") + 1)
  let feedLinks = {
    json: link + "json",
    rss: link + "rss",
    atom: link + "atom"
  }
  const feed = new Feed({
    title: "DataWars2 Feeds",
    description: "Feeds for DataWars2",
    id: link,
    link: link,
    language: "en",
    favicon: "https://datawars2.ie/favicon.ico",
    copyright: "BlueOak-1.0.0",
    feedLinks: feedLinks,
    author: {
      name: "DataWars2",
      email: "support@datawars2.ie",
      link: "https://datawars2.ie"
    }
  })

  let latestFifty: any[] = await dbManager.get_items(collection, {} ,undefined, 50, 0, defaultSort);

  let newLine = "\n"
  switch(type){
    case "json":{newLine = "\n"; break}
    case "atom":{newLine = "<br />"; break}
    case "rss":{newLine = "<br />"; break}
  }

  if(link.indexOf("catalogue") !== -1){
    latestFifty.forEach(item => {
      let img = "https://services.staticwars.com/gw2/img/content/" + item.imageHash +"_large.png"
      let content = ""

      if(item.name){
        content += "New Gemstore item: "+ item.name
        content += newLine
        content += newLine
      }

      if(item.blurb){
        content += item.blurb.replace(/\r\n|\r|\n/g, newLine);
        content += newLine
        content += newLine
      }

      if(item.description){
        content += item.description.replace(/\r\n|\r|\n/g, newLine);
        content += newLine
        content += newLine
      }

      if(item.latestStartDate){
        content += "Start Date: " + new Date(item.latestStartDate).toUTCString()
        content += newLine
      }

      if(item.latestEndDate){
        content += "End Date: " + new Date(item.latestEndDate).toUTCString()
        content += newLine
      }

      let link = "http://api.datawars2.ie/gw2/v1/gemstore/catalogue/json?filter=uuid:" + item.uuid
      if(item.dataId){
        content += "Item ID: " + item.dataId
        content += newLine
        content += "Official API: https://api.guildwars2.com/v2/items?ids=" + item.dataId
        content += newLine
        content += "DataWars2 API: https://api.datawars2.ie/gw2/v1/items/json?filter=id:eq:" + item.dataId
        content += newLine
        link = "https://api.datawars2.ie/gw2/v1/items/json?filter=id:eq:" + item.dataId
      }

      feed.addItem({
        title: item.name,
        id: link,
        description: item.description,
        content: content,
        date: new Date(item.firstAdded),
        image: img,
        link: link
      });
    });
  }

  if(link.indexOf("items") !== -1){
    latestFifty.forEach(item => {
      let img = item.img
      let content = ""
      if(item.name){
        content += "New item: "+ item.name
        content += newLine
        content += newLine
      }

      if(item.description){
        content += item.description.replace(/\r\n|\r|\n/g, newLine);
        content += newLine
        content += newLine
      }

      if(item.id){
        content += "Item ID: " + item.id
        content += newLine
        content += "Official API: https://api.guildwars2.com/v2/items?ids=" + item.id
        content += newLine
        content += "DataWars2 API: https://api.datawars2.ie/gw2/v1/items/json?filter=id:eq:" + item.id
        content += newLine
      }
      let link = "https://api.datawars2.ie/gw2/v1/items/json?filter=id:eq:" + item.id
      if(item.chat_link){
        content += "Wiki: " + "https://wiki.guildwars2.com/wiki/?search=" + encodeURIComponent(item.chat_link)
        content += newLine
        link = "https://wiki.guildwars2.com/wiki/?search=" + encodeURIComponent(item.chat_link)
      }
      if(item.rarity){
        content += "Rarity: " + item.rarity
        content += newLine
      }
      if(item.type){
        content += "Type: " + item.type
        content += newLine
      }
      if(item.weaponType){
        content += "Sub-Type: " + item.weaponType
        content += newLine
      }
      if(item.level){
        content += "Level: " + item.level
        content += newLine
      }
      if(item.binding){
        content += "Binding: " + item.binding
        content += newLine
      }

      feed.addItem({
        title: item.name,
        id: link,
        description: item.description,
        content: content,
        date: new Date(item.firstAdded),
        image: img,
        link: link
      });
    });
  }

  let maxAge = 120
  switch(type){
    case "json":{
      res.sendRaw(200, feed.json1(), { 'Content-Type': 'application/json; charset=utf-8', "Cache-Control": "max-age="+ maxAge })
      break
    }
    case "atom":{
      res.sendRaw(200, feed.atom1(), { 'Content-Type': 'text/xml; charset=utf-8', "Cache-Control": "max-age="+ maxAge })
      break
    }
    case "rss":{
      res.sendRaw(200, feed.rss2(), { 'Content-Type': 'application/rss+xml; charset=utf-8', "Cache-Control": "max-age="+ maxAge })
      break
    }
    default:{
      res.sendRaw(200, beautifyJSON(feedLinks, "human"), { 'Content-Type': 'application/json; charset=utf-8', "Cache-Control": "max-age="+ maxAge })
    }
  }
}