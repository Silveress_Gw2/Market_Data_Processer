/*
  This handles gets data from external repos and spreadsheets
*/

import axios from "axios"
import {db, googleSheets} from "../config/config"

import { DB_Manager } from "../config/db"
import {All_Info, Festivals, Raid_Marketable, Recipes} from "../config/db_entities"

let misc_spreadsheet_id = '1yv-NoymsEeEd4NKj8JNM5o0NP2i5dsyxcQy1ohEcJrg'

// logging the messages for teh end
let end_messages = [];

controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  let timer = Date.now();

  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  // run all of these in parallel
  await Promise.allSettled([
    get_raid(dbManager),
    get_festival_four_winds(dbManager),
    get_recipes_mystic_forge(dbManager)
  ]).catch(err => console.log(err))

  await dbManager.down();

  let total_time =  (Date.now() - timer) / 1000;
  end_messages.push({migration: "Total", items: 0, time: total_time})
  console.table(end_messages)
}

interface Spreadsheet_Response {
  values: string[][]
}

async function get_raid (dbManager: DB_Manager) {
  let timer = Date.now();

  let url = `https://sheets.googleapis.com/v4/spreadsheets/${misc_spreadsheet_id}/values/Raids?key=${googleSheets}`
  // does not matter if it errors, still can be "processed"
  let response: {data: Spreadsheet_Response} = await axios.get(encodeURI(url)).catch((err)=> {console.log(err);return {data:{values:[]}}})

  let dataArray: Raid_Marketable[] = []
  let result = response.data
  let headers = result.values[0]
  for (let i = 1; i < result.values.length; i++) {
    let tmp: Raid_Marketable = {
      row: i,
      id: 0,
      cost: 0,
      gold: 0,
      currency: "",
    }
    let row = result.values[i]
    for (let j = 0; j < headers.length; j++) {
      let value = row[j]
      switch(headers[j]){
        case "ID" :{
          tmp.id = parseInt(value, 10);
          break;
        }
        case "Cost" :{
          tmp.cost = parseInt(value, 10);
          break;
        }
        case "Gold" :{
          tmp.gold = parseInt(value, 10) * 10000;
          break;
        }
        case "Currency" :{
          tmp.currency = value;
          break;
        }
        default: {
          // Name will fall into here
          break;
        }
      }
    }
    dataArray.push(tmp)
  }

  if (dataArray.length > 0) {
    await dbManager.update_bulk(Raid_Marketable, dataArray);
  }

  let total_time =  (Date.now() - timer) / 1000;
  console.log(`get_raid: ${dataArray.length} items in ${total_time}s`)
  end_messages.push({migration: "get_raid", items: dataArray.length, time: total_time})
}

async function get_festival_four_winds (dbManager: DB_Manager) {
  let timer = Date.now();
  let url = `https://sheets.googleapis.com/v4/spreadsheets/${misc_spreadsheet_id}/values/Four_Winds_Box?key=${googleSheets}`
  // does not matter if it errors, still can be "processed"
  let response: {data: Spreadsheet_Response} = await axios.get(encodeURI(url)).catch((err)=> {console.log(err);return {data:{values:[]}}})

  let dataArray: Festivals[] = []
  let result = response.data
  let headers = result.values[0]
  for (let i = 1; i < result.values.length; i++) {
    let tmp: Festivals = {
      row: i,
      festival: "",
      type: "",
      year: 0,
      start: "",
      end: "",
      id: 0,
      quantity: 0,
    }
    let row = result.values[i]
    for (let j = 0; j < headers.length; j++) {
      let value = row[j]
      switch(headers[j]){
        case "Festival" :{
          tmp.festival = value;
          break;
        }
        case "Type" :{
          tmp.type = value;
          break;
        }
        case "Year" :{
          tmp.year = parseInt(value, 10);
          break;
        }
        case "Start" :{
          tmp.start = value;
          break;
        }
        case "End" :{
          tmp.end = value;
          break;
        }
        case "ID" :{
          tmp.id = parseInt(value, 10);
          break;
        }
        case "Quantity" :{
          tmp.quantity = parseInt(value, 10);
          break;
        }
        default: {
          // item will fall into here
          break;
        }
      }
    }
    dataArray.push(tmp)
  }

  if (dataArray.length > 0) {
    await dbManager.update_bulk(Festivals, dataArray);
  }


  let total_time =  (Date.now() - timer) / 1000;
  console.log(`get_festival_four_winds: ${dataArray.length} items in ${total_time}s`)
  end_messages.push({migration: "get_festival_four_winds", items: dataArray.length, time: total_time})
}

async function get_recipes_mystic_forge (dbManager: DB_Manager){
  let timer = Date.now();
  let url = "https://raw.githubusercontent.com/gw2efficiency/custom-recipes/master/recipes.json"
  // does not matter if it errors, still can be "processed"
  let forge_result = await axios.get(encodeURI(url)).catch(err => {console.log(err);return err});
  if (typeof forge_result === 'undefined'  || typeof forge_result.data === 'undefined') { return }


  let forge_data: Recipes[] = forge_result.data
  // do a small bit of tidy up
  for(let i=0;i<forge_data.length;i++){
    // give ID as an inverse of its position
    forge_data[i].id = (i+1) * -1
    // set type
    forge_data[i].type = "MysticForge"
  }
  await dbManager.update_bulk(Recipes, forge_data);


  let total_time =  (Date.now() - timer) / 1000;
  console.log(`get_recipes_mystic_forge: ${forge_data.length} items in ${total_time}s`)
  end_messages.push({migration: "get_recipes_mystic_forge", items: forge_data.length, time: total_time})

  timer = Date.now();


  // buying from merchant does not count as craftable in my book
  let filtered = forge_data.filter(item => item.disciplines.indexOf("Merchant") === -1);
  let results_all_info = filtered.map((item) => {
    let tmp: Partial<All_Info> = {
      id: item.output_item_id,
      craftable: true,
    }
    return tmp;
  })
  await dbManager.update_bulk(All_Info, results_all_info, undefined, undefined, { firstAdded: new Date().toISOString() });

  let total_time_all_info = (Date.now() - timer) / 1000;
  console.log(`recipes_mystic_forge_all_info: ${forge_data.length} items in ${total_time_all_info}s`)
  end_messages.push({migration: `recipes_mystic_forge_all_info`, items: forge_data.length, time: total_time_all_info})
}