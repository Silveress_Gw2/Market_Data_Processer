
import { DB_Manager } from "../config/db"
import { Item, Item_Stats, All_Info } from "../config/db_entities"
import { array_to_keyed_object, getDarthsCDN } from "./functions"
import { db } from "../config/config";

controller().catch((err) => console.error(err));
async function controller() {
  let dbManager = new DB_Manager();
  await dbManager.lift(db.uri, db.database)

  let items = await dbManager.get_items<Item>(Item, {})

  let itemStats = await dbManager.get_items<Item_Stats>(Item_Stats, {}).then(result => array_to_keyed_object(result, "id"))

  let processed = processItemData (items, itemStats)

  await dbManager.update_bulk<All_Info>(All_Info, processed, undefined, undefined, { firstAdded: new Date().toISOString() })

  await dbManager.down();
}

function processItemData (items: Item[], stats: {[p: string]: Item_Stats}): Partial<All_Info>[] {
  let result:Partial<All_Info>[] = []
  for(let i=0;i<items.length;i++){
    let item = items[i]

    let itemData:Partial<All_Info> = {}
    //itemData.lastUpdate = item.lastUpdate
    itemData.id = item.id
    itemData.name = item.name
    itemData.level = item.level
    itemData.img = getDarthsCDN(item.icon)
    itemData.img_official = item.icon
    itemData.type = item.type
    itemData.rarity = item.rarity
    itemData.vendor_value = item.vendor_value
    itemData.chat_link = item.chat_link
    itemData.description = item.description
    itemData.default_skin = item.default_skin
    itemData.AccountBound = false
    if (item.details) {
      itemData.weaponType = item.details.type
      if (item.details.infix_upgrade && item.details.infix_upgrade.id && item.details.infix_upgrade.id !== '') {
        itemData.statID = item.details.infix_upgrade.id
        if (stats[itemData.statID]) {
          itemData.statName = stats[itemData.statID].name
        }
      }
      if (item.details.suffix_item_id && item.details.suffix_item_id !== '') {
        itemData.upgrade1 = item.details.suffix_item_id - 0 || 0
      }
      if (item.details.secondary_suffix_item_id && item.details.secondary_suffix_item_id !== '') {
        itemData.upgrade2 = item.details.secondary_suffix_item_id - 0 || 0
      }

      if(item.details.weight_class){
        itemData.weight = item.details.weight_class
      }
    }
    itemData.binding = 'None'
    if (item.flags) {
      for (let ii = 0; ii < item.flags.length; ii++) {
        itemData[item.flags[ii]] = true
        if (item.flags[ii] === 'AccountBound') {
          itemData.binding = 'Account'
        }
        if (item.flags[ii] === 'SoulbindOnAcquire') {
          itemData.binding = 'Soul'
        }
      }
    }
    if (item.description && item.description.indexOf('Element:') !== -1) {
      itemData.charm = item.description.slice(item.description.indexOf('c>') + 2, item.description.indexOf('<br'))
    }

    for (let propName in itemData) {
      if (itemData[propName] === null || itemData[propName] === undefined || itemData[propName] === '') {
        delete itemData[propName]
      }
    }
    result.push(itemData)
  }
  return result
}
